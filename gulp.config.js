const templateName = 'przepisy';
const templatePathDist = `./wp_html/wp-content/themes/${templateName}`;

const dirs = {
    src: './src',
    dist: `${templatePathDist}`
};

const paths = {
    src: {
        template: `${dirs.src}/${templateName}`,
        styles: `${dirs.src}/assets/styles/**/*.scss`,
        scripts: `${dirs.src}/assets/scripts/**/*.js`,
        images: `${dirs.src}/assets/images/**/*`,
        spriteImg: `${dirs.src}/assets/sprite-img/**/*`,
        spriteSvg: `${dirs.src}/assets/sprite-svg/**/*`
    },
    dist: {
        styles: `${dirs.dist}/styles/`,
        scripts: `${dirs.dist}/scripts/`,
        images: `${dirs.dist}/images/`
    }
};

const bundles = {
    app: `${dirs.src}/assets/scripts/main.js`,
    vendor: [
        'bootstrap-sass',
    ]
};

module.exports = {
    dirs,
    paths,
    bundles
};
