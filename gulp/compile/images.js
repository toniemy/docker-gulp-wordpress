module.exports = function (gulp, gulpConfig, callback) {
    return gulp.src(gulpConfig.paths.src.images)
        .pipe(gulp.dest(gulpConfig.paths.dist.images));
};
