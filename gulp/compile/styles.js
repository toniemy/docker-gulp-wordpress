const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const cssnano = require('cssnano');
const autoprefixer = require('autoprefixer');

module.exports = function (gulp, gulpConfig, callback) {
    var processors = [
        autoprefixer({
            browsers: ['last 1 version']
        }),
        cssnano()
    ];
    return gulp.src(gulpConfig.paths.src.styles)
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(gulp.dest(gulpConfig.paths.dist.styles));
};
