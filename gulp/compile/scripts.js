const browserSync = require('browser-sync');
const webpack = require('webpack');
const gulpWebpack = require('webpack-stream');

module.exports = function (gulp, gulpConfig, callback) {
    return gulp.src(gulpConfig.paths.src.scripts)
        .pipe(gulpWebpack({
            entry: gulpConfig.bundles,
            output: {
                filename: 'app.bundle.js'
            },
            module: {
                loaders: [
                    {
                        test: /src.*\.js$/,
                        exclude: /node_modules/,
                        loader: 'babel-loader?presets[]=es2015'
                    }
                ]
            },
            plugins: [
                new webpack.ProvidePlugin({
                   $: 'jquery',
                   jQuery: 'jquery'
                }),
                new webpack.optimize.OccurenceOrderPlugin(),
                new webpack.optimize.DedupePlugin(),
                new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.bundle.js'),
                new webpack.optimize.UglifyJsPlugin({
                    compress: {
                        drop_console: true,
                        warnings: true
                    }
                })
            ]
        }, webpack))
        .pipe(gulp.dest(gulpConfig.paths.dist.scripts));
};
