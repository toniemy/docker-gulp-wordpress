const spritesmith = require('gulp.spritesmith');

module.exports = function (gulp, gulpConfig, callback) {
    var config = {
        imgName: 'sprite-img.png',
        imgPath: '../images/sprite-img.png',
        cssName: '../styles/_sprite-img.scss'
    };

    return gulp.src(gulpConfig.paths.src.spriteImg)
        .pipe(spritesmith(config)).on('error', function (error) {
            console.log(error);
        })
        .pipe(gulp.dest(`${gulpConfig.dirs.src}/assets/images/`));
};
