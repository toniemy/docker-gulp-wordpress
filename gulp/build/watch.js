const browserSync = require('browser-sync');

module.exports = function (gulp, gulpConfig, callback) {
    browserSync({
        proxy: '192.168.99.100:8000',
        open: 'external'
    });

    gulp.watch(gulpConfig.paths.src.styles, ['build:styles']);
    gulp.watch(gulpConfig.paths.src.scripts, ['build:scripts']);
    gulp.watch(gulpConfig.paths.src.template + '/**/*.php', ['build:template']);
};
