const browserSync = require('browser-sync');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');

module.exports = function (gulp, gulpConfig, callback) {
    var processors = [
        autoprefixer({
            browsers: ['last 1 version']
        })
    ];
    return gulp.src(gulpConfig.paths.src.styles)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(gulpConfig.paths.dist.styles))
        .pipe(browserSync.reload({stream: true}));
};
