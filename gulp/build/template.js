const browserSync = require('browser-sync');

module.exports = function (gulp, gulpConfig, callback) {
    return gulp.src(gulpConfig.paths.src.template + '/**/*')
        .pipe(gulp.dest(gulpConfig.dirs.dist))
        .pipe(browserSync.reload({stream: true}));
};
