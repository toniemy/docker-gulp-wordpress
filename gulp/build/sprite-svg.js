const svgSprite = require('gulp-svg-sprite');

module.exports = function (gulp, gulpConfig, callback) {
    var config = {
        "mode": {
            "symbol": {
                "dest": "",
                "sprite": "sprite-svg.svg"
            }
        }
    };

    return gulp.src(gulpConfig.paths.src.spriteSvg)
        .pipe(svgSprite(config)).on('error', function (error) {
            console.log(error);
        })
        .pipe(gulp.dest(`${gulpConfig.dirs.src}/assets/images/`));
};
