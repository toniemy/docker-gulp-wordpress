const del = require('del');

module.exports = function (gulp, gulpConfig, callback) {
    return del([gulpConfig.dirs.dist]);
};
