const gulp = require('gulp');
const gulpconfig = require('./gulp.config');
const gulpRequireTasks = require('gulp-require-tasks');
const runSequence = require('run-sequence');

gulpRequireTasks({
    path: __dirname + '/gulp',
    arguments: [gulpconfig]
});

gulp.task('sprite:img', function (cb) {
    return runSequence(
        'build:sprite-img',
        cb
    );
});

gulp.task('sprite:svg', function (cb) {
    return runSequence(
        'build:sprite-svg',
        cb
    );
});

gulp.task('build', function (cb) {
    return runSequence(
        'build:clean',
        'build:template',
        ['build:styles', 'build:scripts', 'build:images'],
        cb
    );
});

gulp.task('build:serve', function (cb) {
    return runSequence(
        'build',
        'build:watch',
        cb
    );
});

gulp.task('compile', function (cb) {
    return runSequence(
        'compile:clean',
        'compile:template',
        ['compile:styles', 'compile:scripts', 'compile:images'],
        cb
    );
});

gulp.task('compile:serve', function (cb) {
    return runSequence(
        'compile',
        'compile:watch',
        cb
    );
});

gulp.task('default', ['compile'], function () {

});
