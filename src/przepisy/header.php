<?php
/**
 * The header for our theme.
 *
 * @package Przepisy
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=latin-ext" rel="stylesheet">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header role="banner">
        <?php if ( is_home() ) : ?>
            <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Strona główna"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/icons.svg#cooker" /></svg></a></h1>
        <?php else : ?>
            <p><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Strona główna"><svg class="icon"><use xlink:href="<?php echo get_template_directory_uri(); ?>/icons.svg#cooker" /></svg></a></p>
        <?php endif; ?>

        <?php
            wp_nav_menu( array(
                'theme_location' => 'primary',
                'menu_id' => 'primary-menu',
                'container' => 'nav',
                'menu_class' => 'header__nav'
            ) );
        ?>
    </header>
